checksum-all.sh
===============

This script helps me with doing checksums of everything backup-related in order to periodically check file integrity.

Could be used for other stuff I suppose.

Usage:
```
Syntax: checksum_all.sh [option]

Options:
-h            Display a help message

-c            Find files starting from $(pwd), create sha256sum
              for each file and add that checksum to a file

-u [file]     Find files and if they're not in the supplied
              checksum file, hash them and append them to the file

-v [file]     Verify hashes in supplied file
```

Sample outputs:

```bash
[user @ machine in ~] $ ./checksum_all.sh -c
[+] Running...
[+] Checksums created successfully. Results output to "2018-Mar-09_1322.sha256sum"

[user @ machine in ~] $ cat 2018-Mar-09_1322.sha256sum 
d85fb234da68542ec0549f243c1d247c4128ff2e8f37b7e2573e922426c4240c  ./2018-Mar-09_1322.sha256sum
4355a46b19d348dc2f57c046f8ef63d4538ebb936000f3c9ee954a27460dd865  ./test1

[user @ machine in ~] $ echo 2 > test2
[user @ machine in ~] $ ./checksum_all.sh -u 2018-Mar-09_1322.sha256sum 
[+] Updating checksums file for not already present files...
[+] Updating 2018-Mar-09_1322.sha256sum...
[+] Finished updating "2018-Mar-09_1322.sha256sum" checksums file for new files to hash.

[user @ machine in ~] $ cat 2018-Mar-09_1322.sha256sum 
d85fb234da68542ec0549f243c1d247c4128ff2e8f37b7e2573e922426c4240c  ./2018-Mar-09_1322.sha256sum
4355a46b19d348dc2f57c046f8ef63d4538ebb936000f3c9ee954a27460dd865  ./test1
53c234e5e8472b6ac51c1ae1cab3fe06fad053beb8ebfd8977b010655bfdd3c3  ./test2

[user @ machine in ~] $ ./checksum_all.sh -v 2018-Mar-09_1322.sha256sum 
[+] Checksums verified successfully. Results output to "verified_2018-Mar-09_1325.results".
[user @ machine in ~] $ cat verified_2018-Mar-09_1325.results 
```
