#!/bin/bash
#
# Description: These two functions imitate the behaviour of cron or at. Useful when you
#              either don't have access to cron or at, or want to schedule script runs
#              on your own, via bash.
#
# Run information: This file should be imported by sourcing it with . <path_to_this_file>
#                  and then calling the appropriate functions with a positional parameter,
#                  documented below.
#

function schedule_at(){
    #
    # This function takes as it's only parameter a 24 military hour format time,
    # e.g. 0600 or 2300 and will sleep for the exact number of seconds until then.
    # You can use it in a loop, as such:
    # 
    # while true; do
    #    schedule_at "0600"
    #    <code>
    # done
    #
    # NOTE: make sure you quote the parameter; unforseen consequences might occur otherwise.
    #
    TARGET_HOUR=$1
    NOW=$(date +%s)
    TARGET_TIME=$([ $(date +%H%M) -lt "$TARGET_HOUR" ] && date -d "$TARGET_HOUR" +%s || date -d "tomorrow $TARGET_HOUR" +%s )
    TIMER=$(( $TARGET_TIME - $NOW ))
    sleep $TIMER
}


function schedule_delay(){
    #
    # This function takes as it's only parameter a time in the string format,
    # e.g. "1 minute(s)", "3 hour(s)", "1 day(s) 3 hour(s)" etc. Strings can be
    # both expressed in the singular or plural
    # You can use it in a loop, as such:
    #
    # while true; do
    #    schedule_delay "1 minute"
    #    <code>
    # done 
    #
    # NOTE: make sure you quote the parameter
    #
    TARGET_HOUR=$1
    NOW=$(date +%s)
    TARGET_TIME=$(date -d "$TARGET_HOUR" +%s)
    TIMER=$(( $TARGET_TIME - $NOW ))
    sleep $TIMER
}
