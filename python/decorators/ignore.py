#!/usr/bin/env python

from contextlib import contextmanager

# Import this and then use ignored as e.g.:
# with ignored(IOError):
#     ...

@contextmanager
def ignored(*exceptions):
    try:
        yield
    except exceptions:
        pass
