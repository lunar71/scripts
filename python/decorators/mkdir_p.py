"""
This decorator simulates the UNIX mkdir -p action,
of creating an entire directory path if it doesn't exist.

If the encountered exception is not the one that says the
directory already exists, then there must be another issue
at hand, and that exception will be raised.

Example:

p = '~/python/test_dir'

@mkdir_p
def test(path):
    expanded_path = os.path.expanduser(p)
    with open(os.path.join(expanded_path, 'hello_world.txt'), 'w') as fh:
        fh.write('Hello, World!\n')

test(p)

"""
#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import errno


def mkdir_p(func):
    """ Decorator name. """
    def wrapper(path):
        """ Wrapper function. """
        # Try to expand any tilde (~) in the given path.
        expanded_path = os.path.expanduser(path)
        try:
            # Recursive directory creation function.
            os.makedirs(expanded_path)
        except OSError as exc:
            # Will return an exception if the leaf directory
            # already exists or cannot be created
            # Will also avoid a race condition.
            if exc.errno != errno.EEXIST:
                raise
        return func(expanded_path)
    return wrapper
