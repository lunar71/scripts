#!/usr/bin/env python
# -*- coding: utf-8 -*-

import smtplib
import argparse
from email.MIMEText import MIMEText
from email.MIMEMultipart import MIMEMultipart

class MailPy(object):
    def __init__(self, mailserver, port, username, password, to, subject, body):
        self.mailserver = smtplib.SMTP(mailserver, port)
        self.username = username
        self.password = password
        self.to = to
        self.subject = subject
        self.body = body
        self.mail_sig = '\n\n---\nSent with mail.py'

    def _setup_mail(self):
        try:
            msg = MIMEMultipart()
            msg['From'] = self.username
            msg['To'] = self.to
            msg['Subject'] = self.subject

            text = self.body
            text += self.mail_sig

            msg.attach(MIMEText(text, 'plain'))
            return msg

        except Exception, exc:
            print 'Exception occured while composing message: {}'.format(exc)

    def send_mail(self):
        msg = self._setup_mail()
        try:
            self.mailserver.starttls()
            self.mailserver.login(self.username, self.password)
            self.mailserver.sendmail(self.username, self.to, msg.as_string())

        except smtplib.SMTPException, exc:
            print 'SMTPException occured: {}'.format(exc)

        except Exception, exc:
            print 'Exception occured while trying to send mail: {}'.format(exc)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        usage='%(prog)s -m|--mailserver <smtp@gmail.com>\n\
               -n|--portnumber <mail_port>\n\
               -u|--username   <from_username@example.com>\n\
               -p|--password   <password>\n\
               -t|--to         <to_username@example.com>\n\
               -s|--subject    <mail_subject>\n\
               -b|--body       <mail_body>',

        epilog='Example:\n./mail.py -m smtp.gmail.com -n 587 -u myusername@gmail.com\
-p myp4ssw0rd -t friend@gmail.com -s "Hi there" -b "I sent this with mail.py"',

        formatter_class=argparse.RawDescriptionHelpFormatter,
        add_help=False)

    parser.add_argument('-m', '--mailserver',
                        type=str,
                        help=argparse.SUPPRESS)

    parser.add_argument('-n', '--portnumber',
                        type=str,
                        help=argparse.SUPPRESS)

    parser.add_argument('-u', '--username',
                        type=str,
                        help=argparse.SUPPRESS)

    parser.add_argument('-p', '--password',
                        type=str,
                        help=argparse.SUPPRESS)

    parser.add_argument('-t', '--to',
                        type=str,
                        help=argparse.SUPPRESS)

    parser.add_argument('-s', '--subject',
                        type=str,
                        help=argparse.SUPPRESS)

    parser.add_argument('-b', '--body',
                        type=str,
                        help=argparse.SUPPRESS)

    args = parser.parse_args()

    if not (args.mailserver or args.portnumber or args.username or
            args.password or args.to or args.subject or args.body):
        parser.print_help()

    else:
        MailPy = MailPy(args.mailserver, args.portnumber, args.username,
                        args.password, args.to, args.subject, args.body)
        MailPy.send_mail()
