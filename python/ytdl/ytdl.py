#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import argparse
import requests

try:
    #import youtube_dl
    import yt_dlp as youtube_dl
except ImportError as e:
    print('yt-dlp not found, install with pip3 install yt-dlp')
    sys.exit(1)

try:
    from bs4 import BeautifulSoup
except ImportError as e:
    print('BeautifulSoup not found, install with pip3 install bs4')
    sys.exit(1)


class YTDL(object):
    def __init__(self, url, download_type, output_dir):
        self.opts = {
            'format': 'bestaudio/best',
            'ignoreerrors': True,
            'no_warnings': True,
            'writedescription': True,
            #'limit_rate': '20K',       # rate-limiting
            'outtmpl': '',
            'postprocessors': [{'key': 'FFmpegExtractAudio',
                                'preferredcodec': '{}',
                                'preferredquality': '0'}]
            }

        self.output_dir = output_dir
        self.outtmpl = '{}/%(title)s.%(ext)s'
        self.url = url.rstrip('/')
        self.download_type = download_type


    def _download(self, url):
        with youtube_dl.YoutubeDL(self.opts) as ydl:
            ydl.download([url])


    def _parse_bandcamp_url(self):
        links_list = list()

        s = requests.Session()
        r = s.get(self.url)
        soup = BeautifulSoup(r.content, 'html.parser')
        links = soup.find_all('a')
        self.url = soup.find('meta', property='og:url')['content']

        for link in links:
            href = link.get('href')
            try:
                if (href.startswith('/track') or href.startswith('/album')):
                    links_list.append(href)
            except AttributeError:
                pass

        return links_list


    def download(self):
        if self.download_type == 'bandcamp':
            links_list = self._parse_bandcamp_url()

            archive_name = self.url.split('://')[1]
            self.opts['download_archive'] = '{}-downloaded.log'.format(archive_name)
            self.opts['postprocessors'][0]['preferredcodec'] = 'mp3'

            for link in links_list:
                if 'album' in link:
                    album_name = link.split('/')[2]
                    self.opts['outtmpl'] = self.outtmpl.format(os.path.join(self.output_dir, album_name))

                elif 'track' in link:
                    self.opts['outtmpl'] = self.outtmpl.format(os.path.join(self.output_dir, 'tracks'))

                full_bandcamp_url = '{}{}'.format(self.url, link)
                self._download(full_bandcamp_url)

        elif self.download_type == 'youtube':

            self.opts['outtmpl'] = self.outtmpl.format(self.output_dir)
            self.opts['format'] = 'm4a'
            self.opts['postprocessors'][0]['preferredcodec'] = 'm4a'

            self._download(self.url)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('-y', '--youtube')
    parser.add_argument('-b', '--bandcamp')
    parser.add_argument('-o', '--output-dir', default=os.curdir)

    args = parser.parse_args()

    if args.youtube:
        y = YTDL(args.youtube, 'youtube', args.output_dir)
        y.download()

    elif args.bandcamp:
        y = YTDL(args.bandcamp, 'bandcamp', args.output_dir)
        y.download()

    else:
        parser.print_help()
